import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as HttpStatus from 'http-status-codes';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { Logout } from './auth/auth.actions';


@Injectable()
export class AppErrorHandler implements ErrorHandler {

  constructor(
    private injector: Injector,
  ) { }


  handleError(error: any): void {
    if (error instanceof HttpErrorResponse) {
      this.handleHttpError(error as HttpErrorResponse);
    } else if (error instanceof Error) {
      this.showUnexpectedError((error as Error).message);
    } else {
      this.showUnexpectedError(error.toString());
    }
    throw error;
  }

  handleHttpError(error: HttpErrorResponse): void {
    const store: Store = this.injector.get(Store);

    switch (error.status) {
      case HttpStatus.UNAUTHORIZED:
        this.showUnexpectedError('Your login has expired.  You must log in again to continue');
        store.dispatch(new Logout());
        break;
      case HttpStatus.NOT_FOUND:
        this.showUnexpectedError('Unexpected error when interacting with the server');
        break;
      case HttpStatus.INTERNAL_SERVER_ERROR:
        this.showUnexpectedError('There was an internal server error');
        break;
      case 0:
        this.showUnexpectedError('The server is down/unresponsive.  Please try again later');
        break;
      default:
        console.log('Default Htpp Error', error);
        this.showUnexpectedError(`Error when calling the server: ${error.message}`);
        break;
    }
  }

  showUnexpectedError(message: string): void {
    const toastrService: ToastrService = this.injector.get(ToastrService);
    toastrService.error(
      message,
      'An error occured while processing your request',
      {
        disableTimeOut: true,
        tapToDismiss: true,
        positionClass: 'toast-top-center',
      },
    );
  }

}
