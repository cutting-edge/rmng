import { JWT } from '../auth/auth.token';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {take, switchMap} from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { AuthState } from '../auth/auth.state';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private store: Store) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: JWT | null = this.store.selectSnapshot(AuthState.token);

    //
    // If we have an auth token, inject it into the header
    //
    if (token !== null) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(req);
  }

}
