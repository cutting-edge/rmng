import { OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';


export abstract class AbstractComponent implements OnDestroy {

  subscriptions: Subscription[];

  constructor() {
    this.subscriptions = [];
  }

  ngOnDestroy() {
    this.subscriptions.forEach( (subscription: Subscription) => subscription.unsubscribe() );
  }

  /**
   * Manage the subscription that is passed.  It will be unsubscribed when the component's ngOnDestroy is called.
   *
   * @param s - the subscription to manage
   */
  protected manageSubscription(s: Subscription): void {
    this.subscriptions.push(s);
  }

  /**
   * Subscribe to the observable so that something is responding to (draining) it.
   *
   * @param o - the observable to subscribe to.
   */
  protected subscribe(o: Observable<any>): void {
    this.manageSubscription(o.subscribe( _x => null));
  }
}
