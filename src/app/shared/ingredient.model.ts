export class Ingredient {
  constructor(
    public id: number,
    public name: string,
    public amount: number,
    public userId: number,
    public recipeId: number,
  ) {}

}

export function defaultIngredient(): Ingredient {
  return new Ingredient(undefined, '', 0, undefined, undefined);
}
