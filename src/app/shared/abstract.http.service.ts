import { environment } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';

export abstract class AbstractHttpService {
  //
  // protected fields
  //
  protected standardHttpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    }),
    reportProgress: false,
    withCredentials: false
  };

  //
  // Protected methods
  //
  protected url(path: string | string[]): string {
    const effectivePath = (typeof(path) === 'string') ? path : (path as string[]).join('/');
    return `${environment.appServerPrefix}/${effectivePath}`;
  }
}
