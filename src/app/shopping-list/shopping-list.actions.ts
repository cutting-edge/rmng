import { Ingredient } from '../shared/ingredient.model';
//
// Actions for the shopping list root store
//

export class LoadShoppingList {
  static type = '[Shopping List] - load';
}

export class NewIngredient {
  static type = '[Shopping List] - new ingredient';
}

export class SelectIngredient {
  static type = '[Shopping List] - select ingredient';

  constructor(public id: number) {}
}

export class SaveIngredient {
  static type = '[Shopping List] - save ingredient';

  constructor(public ingredient: Ingredient) {}
}

export class AddIngredientList {
  static type = '[Shopping List] - add ingredient list';
  ingredients: Ingredient[];
}

export class RemoveIngredient {
  static type = '[Shopping List] - remove ingredient';

  constructor(public id: number) {}
}
