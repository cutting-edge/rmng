import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AbstractComponent } from '../shared/abstract.component';
import { LoadShoppingList, NewIngredient } from './shopping-list.actions';
import { ShoppingListStateModel } from './shopping-list.model';
import { ShoppingListState } from './shopping-list.state';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent extends AbstractComponent implements OnInit {
  @Select(ShoppingListState) shoppingListState$: Observable<ShoppingListStateModel>;

  constructor(
    private store: Store,
    private router: Router,
    private route: ActivatedRoute,
   ) {
    super();
  }

  ngOnInit() {
    //
    // Load the shopping list. If the operation fails, navigate back to the root
    //
    this.subscribe(
      this.store.dispatch(new LoadShoppingList()).pipe(
        catchError(err => this.router.navigate(['/']))));
  }

  onNewItem() {
    this.store.dispatch(new NewIngredient());
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
