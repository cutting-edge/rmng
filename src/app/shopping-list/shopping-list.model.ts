import { Ingredient } from '../shared/ingredient.model';

//
// This represents the state model for the shopping-list store root element
//

export interface ShoppingListStateModel {
  ingredients: Ingredient[];
}
