import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { AbstractComponent } from '../../shared/abstract.component';
import { Ingredient } from '../../shared/ingredient.model';
import { NewIngredient, RemoveIngredient, SaveIngredient, SelectIngredient } from '../shopping-list.actions';
import { ShoppingListState } from '../shopping-list.state';


@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent extends AbstractComponent implements OnInit {
  form: FormGroup;
  selectedIngredient$ = ShoppingListState.selectedIngredient$;

  ingredient: Ingredient;

  constructor(
    private store: Store,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super();
   }

  ngOnInit() {
    //
    // Build the form, we want this in place before subscribing
    //
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      amount: new FormControl(null, [Validators.required, Validators.pattern('^[1-9]+[0-9]*$')])
    });

    this.manageSubscription(
      this.selectedIngredient$.subscribe(
        (ingredient: Ingredient | undefined) => {
          if (ingredient === undefined) {
            this.router.navigate(['shopping-list']);
          } else {
            this.ingredient = ingredient;
            this.form.setValue( {
              name: ingredient.name,
              amount: ingredient.amount
            } );
          }
        }));

    this.manageSubscription(this.route.params.subscribe(
        (params: Params) => {
          const idParam: string | undefined = params['id'];
          if (idParam === undefined) {
            this.onClear();
          } else {
            const id: number = +idParam;
            this.store.dispatch(new SelectIngredient(id));
          }
        }));
  }

  editMode(): boolean {
    return this.ingredient.id !== undefined;
  }

  onSubmit(form: NgForm) {
    const value = this.form.value;
    this.ingredient = {...this.ingredient, ...this.form.value };
    this.store.dispatch(new SaveIngredient(this.ingredient));
  }

  onClear() {
    this.store.dispatch(new NewIngredient());
  }

  onDelete() {
    this.manageSubscription(
      this.store.dispatch(new RemoveIngredient(this.ingredient.id)).subscribe(
        (_x: void): void  => {
          this.router.navigate(['shopping-list']);
        }));
  }
}
