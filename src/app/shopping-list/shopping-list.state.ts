import { Action, State, StateContext, Store } from '@ngxs/store';
import { BehaviorSubject, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Logout } from '../auth/auth.actions';
import { Ingredient, defaultIngredient } from '../shared/ingredient.model';
import { AddIngredientList, LoadShoppingList, NewIngredient, RemoveIngredient,
  SaveIngredient, SelectIngredient } from './shopping-list.actions';
import { ShoppingListStateModel } from './shopping-list.model';
import { ShoppingListService } from './shopping-list.service';


function defaultShoppingListState(): ShoppingListStateModel {
  return {
    ingredients: [],
  };
}

@State<ShoppingListStateModel>({
  name: 'shoppingList',
  defaults: defaultShoppingListState(),
})
export class ShoppingListState {

  //
  // create a 'side subject' for the selected ingrediiient
  //
  static selectedIngredient$: BehaviorSubject<Ingredient | undefined> = new BehaviorSubject<Ingredient | undefined>(undefined);

  //
  // get the store and the service via injection
  //
  constructor(private store: Store, private shoppingListService: ShoppingListService) {}

  @Action(LoadShoppingList)
  loadShoppingList(ctx: StateContext<ShoppingListStateModel>) {
    return this.shoppingListService.loadShoppingList().pipe(
      tap ( (ingredients: Ingredient[]) => {
        ctx.patchState({ ingredients });
      }),
    );
  }

  @Action(NewIngredient)
  newIngredient(ctx: StateContext<ShoppingListStateModel>) {
    ShoppingListState.selectedIngredient$.next(defaultIngredient());
  }

  @Action(SelectIngredient)
  selectIngredient(ctx: StateContext<ShoppingListStateModel>, action: SelectIngredient) {
    const ingredients: Ingredient[] = ctx.getState().ingredients;
    const selected: Ingredient = ingredients.find( (i: Ingredient) => (i.id === action.id) );
    ShoppingListState.selectedIngredient$.next(selected);
  }

  @Action(SaveIngredient)
  saveIngredient(ctx: StateContext<ShoppingListStateModel>, action: SaveIngredient) {
    if (action.ingredient.id === undefined) {
      //
      // We're creating a new ingredient, when we get the new one back, it should be
      // the seleted ingredient.
      //
      return this.shoppingListService.createIngredient(action.ingredient).pipe(
        tap( (ing: Ingredient) => {
          ShoppingListState.selectedIngredient$.next(ing);
          this.store.dispatch(new LoadShoppingList());
        }));

    } else {
      //
      // We're updating an existing ingredient
      //
      return this.shoppingListService.updateIngredient(action.ingredient).pipe(
        tap( (x: void) => this.store.dispatch(new LoadShoppingList()) ));
    }
  }

  @Action(AddIngredientList)
  addIngredientList(ctx: StateContext<ShoppingListStateModel>) {

  }

  @Action(RemoveIngredient)
  removeIngredient(ctx: StateContext<ShoppingListStateModel>, action: RemoveIngredient) {
    //
    // If the delete succeeds reset the seleted ingredient and reload the ingredient list
    //
    return this.shoppingListService.deleteIngredient(action.id).pipe(
      tap( (_x: void) => {
        ShoppingListState.selectedIngredient$.next(undefined);
        this.store.dispatch(new LoadShoppingList());
      }));
  }

  @Action(Logout)
  logout(ctx: StateContext<ShoppingListStateModel>) {
    ShoppingListState.selectedIngredient$.next(undefined);
    return of(ctx.setState(defaultShoppingListState()));
  }

}

