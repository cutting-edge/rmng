import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ShoppingEditComponent } from './shopping-edit/shopping-edit.component';
import { ShoppingListComponent } from './shopping-list.component';
import { ShoppingListService } from './shopping-list.service';
import { ShoppingStartComponent } from './shopping-start/shopping-start.component';



@NgModule({
  declarations: [
    ShoppingListComponent,
    ShoppingStartComponent,
    ShoppingEditComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  providers: [
    ShoppingListService
  ]
})
export class ShoppingListModule {}
