import { Ingredient } from '../shared/ingredient.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AbstractHttpService } from '../shared/abstract.http.service';


@Injectable()
export class ShoppingListService extends AbstractHttpService {

  constructor (
    private httpClient: HttpClient,
  ) {
    super();
  }

  loadShoppingList(): Observable<Ingredient[]> {
    const url = this.url('ingredients');

    return this.httpClient.get<Ingredient[]>(url, this.standardHttpOptions);
  }

  updateIngredient(ingredient: Ingredient): Observable<void> {
    const url = this.url(['ingredients', ingredient.id.toString()]);
    return this.httpClient.put<void>(url, ingredient);
  }

  createIngredient(ingredient: Ingredient): Observable<Ingredient> {
    const url = this.url('ingredients');

    return this.httpClient.post<Ingredient>(url, ingredient);
  }

  deleteIngredient(id: number): Observable<void> {
    const url = this.url(['ingredients', id.toString()]);

    return this.httpClient.delete<void>(url);
  }
}
