import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { RecipesState } from '../recipes-state';
import { RecipesStateModel } from '../recipes-state-model';
import { LoadRecipes } from '../recipes-actions';
import { AbstractComponent } from '../../shared/abstract.component';
import { SUPER_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent extends AbstractComponent implements OnInit {
  @Select(RecipesState) recipesState$: Observable<RecipesStateModel>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store) {
    super();
  }

  ngOnInit() {
    this.subscribe(this.store.dispatch(new LoadRecipes()));
  }

  onNewRecipe() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
