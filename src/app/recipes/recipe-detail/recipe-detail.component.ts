import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, takeLast } from 'rxjs/operators';
import { AbstractComponent } from '../../shared/abstract.component';
import { RecipesState } from '../recipes-state';
import { Recipe } from '../recipe.model';
import { Store } from '@ngxs/store';
import { SelectRecipe, DeleteRecipe, AddIngredientsToShoppingList } from '../recipes-actions';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent extends AbstractComponent implements OnInit {
  selectedRecipe$ = RecipesState.selectedRecipe$;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private store: Store,
              private toastrService: ToastrService,
              ) {
      super();
  }


  ngOnInit() {
    this.manageSubscription(this.route.params.subscribe(
      (params: Params): void => {
        const idParam: string | undefined = params['id'];
        const id: number = +idParam;
        this.store.dispatch(new SelectRecipe(id));
      }
    ));
  }

  onAddToShoppingList() {
    this.manageSubscription(
      this.selectedRecipe$.pipe(take(1)).subscribe( (recipe: Recipe) => {
        this.manageSubscription(this.store.dispatch(new AddIngredientsToShoppingList(recipe.id)).subscribe ( (_x: void): void => {
          this.toastrService.success(
            'The recipe\'s ingredients have been added to your shopping list.',
            'To Shipping List',
            { positionClass: 'toast-top-center'},
          );
          this.router.navigate(['/recipes']);
        }));
    }));
  }

  onEditRecipe() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeleteRecipe() {
    this.manageSubscription(
      this.selectedRecipe$.pipe(take(1)).subscribe( (recipe: Recipe) => {
        this.manageSubscription(this.store.dispatch(new DeleteRecipe(recipe.id)).subscribe ( (_x: void): void => {
          this.router.navigate(['/recipes']);
        }));
    }));
  }
}
