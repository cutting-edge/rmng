import { Ingredient } from '../shared/ingredient.model';

export class Recipe {

  constructor(
    public id: number,
    public userId: number,
    public name: string,
    public description: string,
    public imagePath: string,
    public ingredients: Ingredient[],
  ) {}
}

export function defaultRecipe(): Recipe {
  return new Recipe (
    undefined,
    undefined,
    '',
    '',
    '',
    [],
  );
}
