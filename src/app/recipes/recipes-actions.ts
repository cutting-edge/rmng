import { Recipe } from './recipe.model';



export class LoadRecipes {
  static type = '[Recipes] - load';
}

export class SelectRecipe {
  static type = '[Recipes] - select';

  constructor(public id: number) {}
}

export class NewRecipe {
  static type = '[Recipes] - new';
}

export class SaveRecipe {
  static type = '[Recipes] - save';

  constructor(public recipe: Recipe) {}

}

export class DeleteRecipe {
  static type = '[Recipes] - delete';

  constructor(public id: number) {}
}

export class AddIngredientsToShoppingList {
  static type = '[Recipes] - addToShoppingList';

  constructor(public id: number) {}
}
