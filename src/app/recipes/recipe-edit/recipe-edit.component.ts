import { SelectRecipe, SaveRecipe, NewRecipe } from './../recipes-actions';
import { Ingredient, defaultIngredient } from './../../shared/ingredient.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { AbstractComponent } from '../../shared/abstract.component';
import { RecipesState } from '../recipes-state';
import { Recipe } from '../recipe.model';
import { Store } from '@ngxs/store';


@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent extends AbstractComponent implements OnInit {
  selectedRecipe$ = RecipesState.selectedRecipe$;

  recipeForm: FormGroup;

  recipe: Recipe | undefined = undefined;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private store: Store,
              ) {
    super();
  }

  /**
   * Build the recipe edit form, subscribe to the selected recipe and
   * subscribe to parameter changes
   */
  ngOnInit() {
    this.recipeForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'imagePath': new FormControl(null, Validators.required),
      'description': new FormControl(null, Validators.required),
      'ingredients': new FormArray([]),
    });

    //
    // Watch the changes to the selected recipe
    //
    this.manageSubscription(this.selectedRecipe$.subscribe(
      (recipe: Recipe): void => {
        this.recipe = recipe;
        this.initForm();
      }
    ));

    //
    // We don't actually have to do anything with the parameter change
    // unless the id different then the one that is currently selected.
    //
    this.manageSubscription(this.route.params.subscribe(
        (params: Params): void => {
          const idString: string | undefined = params.id;
          if (idString === undefined) {
            //
            // New Recipe
            //
            this.store.dispatch(new NewRecipe());
          } else {
            const id = +idString;
            if (id !== this.recipe.id) {
              this.store.dispatch(new SelectRecipe(id));
            }
          }
        }
      ));
  }

  onSubmit() {
    const recipe: Recipe = {...this.recipe, ...this.recipeForm.value};
    this.manageSubscription(
      this.store.dispatch(new SaveRecipe(recipe)).subscribe(
        (_x: void) => {
          this.onCancel();
        }
      ));
  }

  onAddIngredient() {
    (<FormArray>this.recipeForm.get('ingredients')).push(
      this.ingredientToFormGroup(defaultIngredient()));
  }

  onDeleteIngredient(index: number) {
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  private ingredientToFormGroup(ingredient: Ingredient): FormGroup {
    const fg = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'amount': new FormControl(null, [
        Validators.required,
        Validators.pattern(/^[1-9]+[0-9]*$/)
      ])
    });
    fg.patchValue(ingredient);

    return fg;
  }

  private initForm(): void {
    //
    // Initialize the static parts of the form
    //
    this.recipeForm.patchValue(this.recipe);

    const ingredientControls = new FormArray([]);

    //
    // Create the ingredient control array
    //
    for (const ingredient of this.recipe.ingredients) {
      ingredientControls.push(
        this.ingredientToFormGroup(ingredient));
    }

    //
    // Assign the new array
    //
    this.recipeForm.setControl('ingredients', ingredientControls);
  }

  //
  // figure out if we're in edit mode or not
  //
  private editMode(): boolean {
    return (this.recipe !== undefined) && (this.recipe.id !== undefined);
  }

  //
  // used by the template to get the ingradient formarray
  //
  getIngredientFormGroups() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }

}
