import { Recipe } from './recipe.model';


export interface RecipesStateModel {
  recipes: Recipe[];
}
