import { Action, State, StateContext, Store } from '@ngxs/store';
import { BehaviorSubject, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { defaultRecipe, Recipe } from './recipe.model';
import { AddIngredientsToShoppingList, DeleteRecipe, LoadRecipes, NewRecipe, SaveRecipe, SelectRecipe } from './recipes-actions';
import { RecipesService } from './recipes-service';
import { RecipesStateModel } from './recipes-state-model';
import { Logout } from '../auth/auth.actions';


function defaultRecipesState(): RecipesStateModel {
  return {
    recipes: [],
  };
}

@State<RecipesStateModel>({
  name: 'recipes',
  defaults: defaultRecipesState(),
})
export class RecipesState {

  //
  // create a 'side subject' for the selected recipe
  //
  static selectedRecipe$: BehaviorSubject<Recipe> = new BehaviorSubject<Recipe>(defaultRecipe());

  constructor(private store: Store, private recipeService: RecipesService) {}

  /**
   * Load the user's recipes and update the state
   *
   * @param ctx - state context
   */
  @Action(LoadRecipes)
  loadRecipes(ctx: StateContext<RecipesStateModel>) {
    return this.recipeService.loadRecipes().pipe(
      tap( (recipes: Recipe[]) => {
        ctx.patchState( { recipes });
      } ),
    );
  }

  /**
   * Find the recipe by it's id and make it the selected recipe
   *
   * @param id of the recipe to select
   */
  @Action(SelectRecipe)
  selectRecipe(ctx: StateContext<RecipesStateModel>, action: SelectRecipe) {
    const recipes: Recipe[] = ctx.getState().recipes;
    const selected: Recipe = recipes.find( (r: Recipe) => (r.id === action.id) );
    RecipesState.selectedRecipe$.next(selected);
  }

  /**
   *
   * Set to a new recipe
   *
   * @param ctx - store context
   */
  @Action(NewRecipe)
  newRecipe(ctx: StateContext<RecipesStateModel>) {
    RecipesState.selectedRecipe$.next(defaultRecipe());
  }

  @Action(SaveRecipe)
  saveRecipe(ctx: StateContext<RecipesStateModel>, action: SaveRecipe) {
    return this.recipeService.saveRecipe(action.recipe).pipe(
      switchMap( (_recipe: Recipe) => this.store.dispatch(new LoadRecipes()) )
    );
  }

  @Action(DeleteRecipe)
  deleteRecipe(ctx: StateContext<RecipesStateModel>, action: DeleteRecipe) {
    //
    // If the delete succeeds reset the seleted recipe and reload the recipe list
    //
    return this.recipeService.deleteRecipe(action.id).pipe(
      tap( (_x: void) => {
        RecipesState.selectedRecipe$.next(undefined);
        this.store.dispatch(new LoadRecipes());
      }));
  }

  @Action(AddIngredientsToShoppingList)
  addIngredientsToShoppingList(ctx: StateContext<RecipesStateModel>, action: AddIngredientsToShoppingList) {
    return this.recipeService.addIngredientsToShoppingList(action.id).pipe(
      tap( (_x: void) => {
        RecipesState.selectedRecipe$.next(undefined);
        this.store.dispatch(new LoadRecipes());
      }));
  }

  @Action(Logout)
  logout(ctx: StateContext<RecipesStateModel>) {
    RecipesState.selectedRecipe$.next(undefined);
    return of(ctx.setState(defaultRecipesState()));
  }
}
