import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AbstractHttpService } from '../shared/abstract.http.service';
import { Recipe } from './recipe.model';


@Injectable()
export class RecipesService extends AbstractHttpService {

  constructor (
    private httpClient: HttpClient,
  ) {
    super();
  }

  loadRecipes(): Observable<Recipe[]> {
    const url = this.url('recipes');

    return this.httpClient.get<Recipe[]>(url, this.standardHttpOptions);
  }

  saveRecipe(recipe: Recipe): Observable<Recipe> {
    const url = this.url('recipes');

    return this.httpClient.post<Recipe>(url, recipe);
  }

  deleteRecipe(id: number) {
    const url = this.url(['recipes', id.toString()]);
    return this.httpClient.delete<void>(url);
  }

  addIngredientsToShoppingList(id: number) {
    const url = this.url(['recipes', id.toString(), 'addToShoppingList']);
    return this.httpClient.post<void>(url, {});
  }
}
