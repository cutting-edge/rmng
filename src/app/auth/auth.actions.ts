

//
// Auth store actions
//

//
// Login action - login the user via email address and password
//
export class Login {
  static type = '[auth] Login';
  constructor(public username: string, public password: string) {}
}

export class Logout {
  static type = '[auth] Logout';
}
