import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';

import { AuthState } from './auth.state';
import { JWT } from './auth.token';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private store: Store) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const token: JWT = this.store.selectSnapshot(AuthState.token);
    return token != null;
  }
}
