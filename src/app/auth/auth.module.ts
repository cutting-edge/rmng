import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SigninComponent } from './signin/signin.component';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthService } from './auth.service';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    SigninComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    AuthRoutingModule
  ],
  providers: [
    AuthService
  ]
})
export class AuthModule {}
