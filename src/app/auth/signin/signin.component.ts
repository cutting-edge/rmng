import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Login } from '../auth.actions';
import { AuthState } from '../auth.state';
import { AuthStateModel } from '../auth.model';
import { Router } from '@angular/router';
import { withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent {
  @Select(AuthState) authState$: Observable<AuthStateModel>;

  constructor(private store: Store, private router: Router) { }

  onSignin(form: NgForm) {
    const username = form.value.username;
    const password = form.value.password;
    this.store.dispatch(new Login(username, password)).pipe(
      withLatestFrom(this.authState$)).subscribe( ([authStateModel]) => {
        if (authStateModel.auth.authenticated) {
          this.router.navigate(['/']);
        }
    });
  }
}
