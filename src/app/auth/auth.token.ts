export type JWT = string;

export interface AuthToken {
  token: JWT;
}
