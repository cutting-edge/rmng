import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthToken } from './auth.token';
import { AbstractHttpService } from '../shared/abstract.http.service';

@Injectable()
export class AuthService extends AbstractHttpService {

  constructor (
    private httpClient: HttpClient,
  ) {
    super();
  }

    login(name: string, password: string): Observable<AuthToken> {
    const body = {
      username: name,
      password: password
    };

    const url = this.url('auth/login');
    return this.httpClient.post<AuthToken>(url, body, this.standardHttpOptions);
  }

}
