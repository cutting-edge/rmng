
//
// Model elements maintained by the auth store root element
//

export interface AuthStateModel {
  username: string;
  token: string;
  authenticated: boolean;
  responseStatus: number | null;
}
