import { HttpErrorResponse } from '@angular/common/http';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Login, Logout } from './auth.actions';
import { AuthStateModel } from './auth.model';
import { AuthService } from './auth.service';
import { AuthToken, JWT } from './auth.token';


const defaultAuthState: AuthStateModel = {
  username: null,
  token: null,
  authenticated: false,
  responseStatus: null,
};

@State<AuthStateModel>({
  name: 'auth',
  defaults: defaultAuthState,
})
export class AuthState {

  //
  // inject in the store
  //
  constructor (private store: Store, private authService: AuthService) {}

  //
  // We use this selector to get right to the auth token
  //
  @Selector()
  static token(state: AuthStateModel): JWT  {return state.token; }

  @Action(Login)
  login(ctx: StateContext<AuthStateModel>, action: Login) {
    //
    // Use the auth service to authenticate the user
    //
    return this.authService.login(action.username, action.password).pipe(
      tap( (at: AuthToken) => {
        ctx.patchState({
          username: action.username,
          token: at.token,
          authenticated: true,
          responseStatus: null});
      }),
      catchError((err: HttpErrorResponse, loginResult: Observable<AuthToken>) => {
        return of(ctx.patchState( {responseStatus: err.status} ));
      }));
  }

  @Action(Logout)
  logout(ctx: StateContext<AuthStateModel>) {
    return of(ctx.patchState(defaultAuthState));
  }
}
