import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Logout } from '../../auth/auth.actions';
import { AuthStateModel } from '../../auth/auth.model';
import { AuthState } from '../../auth/auth.state';
import { AbstractComponent } from '../../shared/abstract.component';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent extends AbstractComponent {
  @Select(AuthState) authState$: Observable<AuthStateModel>;

  constructor(private store: Store, private router: Router) {
    super();
   }

  onLogout() {
    this .manageSubscription(
      this.store.dispatch(new Logout()).subscribe( (_x: Observable<void>) => {
        this.router.navigate(['/']);
      }));
  }
}
