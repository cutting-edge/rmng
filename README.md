# N6CompleteGuide - Course Project with NGXS

This is the is the final version of the course project re-written to:

  * use [NGXS](https://ngxs.gitbook.io/ngxs/) for state management
  * use a backend server written in [Nest](https://nestjs.com/) (nest js) rather than FireBase.  The code for backend server is [here](https://bitbucket.org/rich_duncan/rmbe/src/master/)
  * implement all application persistent state within the state management actions.  Note that this removes the load/save operations and the associated menu.

The NGXS state mechanism is used to:

  * manage all application state
  * communicate to the backend via side effect

My goal in doing this was to determine the feasibility of writing a backend in Nest, and to better underestand the patterns of development one would use when implementing an Angular 6 front-end with NGXS.

### Notes About the Implementation:

  * The state organization is similar to the NGRX implementation used in the course.  In particular it does not follow the NGXS style-guide with respect to state organization.
  * In both the ingredient and recipe implementation - I choose to use a [BehaviorSubject](http://reactivex.io/rxjs/manual/overview.html#behaviorsubject) to represent the currently selected recipe and currently selected ingredient.  The goal was to give finer grained control over what changed (was it the list of recipes or the currently selected one).  This could have probably been done with (child) state containers in NGXS.
  * I wanted to better understand how best to handle errors returned by the REST calls to the backend.  The login page handles it's own errors, while the rest of the app relies on a global error handler.
  * Most of the components use an abstract class that provides a subscription management capability (so the individual components don't have to manage their own).
  * I tried to pay particular attention to what parts of the architecture are responsible for what parts of the a given action. So for example when a recipe is deleted:
    1. The component dispatches a DeleteRecipe action
    1. The state container handles the action by:
        1. calling a service to delete the recipe (call http-client interactions are done this way to make things more testable). 
        1. Waiting for the delete to be complete (via pipe and tap) before resetting the state by resetting the selected recipe and re-loading the list from the server.
    1. The component subscribes to the dispatch so that when it is complete, it can change the navigation in the context of the component.
